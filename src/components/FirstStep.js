import React from "react";
import { useForm } from "react-hook-form";
import { Form, Button } from "react-bootstrap";
import { motion } from "framer-motion";
const FirstStep = (props)=>{
    const { user } = props;
    const { register, handleSubmit, errors} = useForm({
        defaultValues:{
            first_name: user.first_name,
            last_name: user.last_name
        }
    });
    const onSubmit = (data) => {
        props.updateUser(data);
        props.history.push('/second');
    };
    return (
        <div>
            <h1>First Step Registration</h1>
            <Form className="input-form" onSubmit={handleSubmit(onSubmit)}>
                <motion.div
                  initial={{ x: '-100vw' }}
                  animate={{ x: 0 }}
                  transition={{ stiffness: 150 }}
                 className="col-md-6 offset-md-3">
                    <Form.Group>
                        <Form.Label>First Name</Form.Label>
                        <Form.Control
                            type="text"
                            name="first_name"
                            placeholder="Enter Your First Name"
                            autocomplete="off"
                            className={`errors.first_name?'input-error':''`}
                            ref={
                                register({
                                    required: "Field should not be left empty.",
                                    pattern: {
                                        value: /^[a-zA-Z]+$/,
                                        message: "Enter only letters."
                                    }
                                })
                            }
                        />
                        {errors.first_name && (
                            <p className="errorMsg">{errors.first_name.message}</p>
                        )}
                    </Form.Group>
                    <Form.Group controlId="last_name">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control
                            type="text"
                            name="last_name"
                            placeholder="Enter Your Last Name"
                            autocomplete="off"
                            className={`errors.last_name?'input-error':''`}
                            ref={
                                register({
                                    required: "Field should not be left empty.",
                                    pattern: {
                                        value: /^[a-zA-Z]+$/,
                                        message: "Enter only letters."
                                    }
                                })
                            }
                        />
                        {errors.first_name && (
                            <p className="errorMsg">{errors.last_name.message}</p>
                        )}
                    </Form.Group>
                    <Button type="submit" variant="primary">Next</Button>
                </motion.div>
            </Form>
        </div>
    )
}

export default FirstStep;