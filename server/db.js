const mongoose = require('mongoose');
const dotenv = require('dotenv'); 
dotenv.config({path: './config.env'});
const connectionString=process.env.URI;
mongoose.connect(connectionString, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
});